const fetchGet = (endpoint, customHeaders) => {
  const { accessToken, client, uid } = customHeaders;
  return fetch(`https://empresas.ioasys.com.br/api/v1/${endpoint}`, {
    headers: {
      'access-token': accessToken,
      client,
      uid,
    },
  }).then((response) =>
    response
      .json()
      .then((json) =>
        response.ok ? Promise.resolve(json) : Promise.reject(json)
      )
  );
};

export default fetchGet;
