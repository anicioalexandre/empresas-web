const fetchPost = (endpoint, loginBody) => {
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Origin': '*',
  };
  return fetch(`https://empresas.ioasys.com.br/api/v1/${endpoint}`, {
    method: 'POST',
    body: JSON.stringify(loginBody),
    headers,
  }).then((response) =>
    response.ok ? Promise.resolve(response) : Promise.reject(response)
  );
};

export default fetchPost;
// const data = {
//   email: 'testeapple@ioasys.com.br',
//   password: '12341234',
// };
// const headers = {
//   'Content-Type': 'application/json',
//   'Access-Control-Origin': '*',
// };
// useEffect(() => {
//   fetch('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
//     method: 'POST',
//     body: JSON.stringify(data),
//     headers,
//   }).then((responde) => {
//     for (let pair of responde.headers.entries()) {
//       console.log(pair[0] + ': ' + pair[1]);
//     }
//   });
//   fetch('https://empresas.ioasys.com.br/api/v1/enterprises', {
//     headers: {
//       'access-token': 'GRVRzp7YuCkS3IN0sq5AEw',
//       client: '-O2lFH-L9skksgemTfBxdQ',
//       uid: 'testeapple@ioasys.com.br',
//     },
//   }).then((responde) => responde.json().then((coisa) => console.log(coisa)));
// }, []);
