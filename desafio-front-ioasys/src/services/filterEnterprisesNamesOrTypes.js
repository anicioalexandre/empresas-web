const filterEnterprisesNamesOrTypes = (enterprisesData, inputValue) => {
  let filteredEnterprises = [...enterprisesData];
  if (inputValue)
    filteredEnterprises = filteredEnterprises.filter(
      (enterpriseInfo) =>
        enterpriseInfo.enterprise_name
          .toLowerCase()
          .includes(inputValue.toLowerCase()) ||
        enterpriseInfo.enterprise_type.enterprise_type_name
          .toLowerCase()
          .includes(inputValue.toLowerCase())
    );
  return filteredEnterprises;
};

export default filterEnterprisesNamesOrTypes;
