import React from 'react';
import renderWithRedux from '../helpers/renderWithRedux';
import renderWithRouter from '../helpers/renderWithRouter';
import App from '../App';

describe('testes da página NotFound', () => {
  it('ao passar um rota não existente deve-se acessar a rota NotFound', () => {
    const { getByText } = renderWithRedux(
      renderWithRouter(<App />, ['/rota/não/existente'])
    );
    expect(getByText('Página não encontrada.'));
  });
});
