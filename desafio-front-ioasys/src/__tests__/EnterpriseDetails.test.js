/* eslint-disable jest/no-mocks-import */
import React from 'react';
import { waitFor, fireEvent } from '@testing-library/react';
import renderWithRedux from '../helpers/renderWithRedux';
import renderWithRouter from '../helpers/renderWithRouter';
import enterprisesListMock from '../__mocks__/enterprisesListMock';
import fetchGet from '../services/fetchGet';
import SearchProvider from '../context/SearchContext';
import EnterpriseDetails from '../pages/EnterpriseDetails';

afterEach(() => {
  fetchGet.mockClear();
});

// mockando o useParams para simular a rota para empresa com id igual a 2
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: jest.fn().mockReturnValue({ id: 2 }),
}));

jest.mock('../services/fetchGet');
fetchGet.mockImplementation(() => Promise.resolve(enterprisesListMock));

describe('testes no EnterpriseDetails', () => {
  it('detalhes da empresa 2 aparecem corretamente', async () => {
    const { getByText, getByAltText, getByRole } = renderWithRedux(
      renderWithRouter(
        <SearchProvider>
          <EnterpriseDetails />
        </SearchProvider>
      )
    );
    await waitFor(() => expect(fetchGet).toHaveBeenCalledTimes(1));
    expect(getByAltText('enterprise logo')).toBeInTheDocument();
    expect(getByText('Alpaca Samka SpA')).toBeInTheDocument();
    expect(getByText('Fashion')).toBeInTheDocument();
    expect(getByText('Chile')).toBeInTheDocument();
    expect(getByRole('article')).toHaveTextContent(
      'Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations.'
    );
  });
});
