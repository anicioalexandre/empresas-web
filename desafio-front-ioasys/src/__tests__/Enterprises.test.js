/* eslint-disable jest/no-mocks-import */
import React from 'react';
import { waitFor, fireEvent } from '@testing-library/react';
import renderWithRedux from '../helpers/renderWithRedux';
import renderWithRouter from '../helpers/renderWithRouter';
import enterprisesListMock from '../__mocks__/enterprisesListMock';
import fetchGet from '../services/fetchGet';
import Enterprises from '../pages/Enterprises';
import SearchProvider from '../context/SearchContext';

afterEach(() => {
  fetchGet.mockClear();
});

jest.mock('../services/fetchGet');
fetchGet.mockImplementation(() => Promise.resolve(enterprisesListMock));

describe('testes no Enterprises', () => {
  it('requisição de empresas acontece corretamente', async () => {
    const { getAllByTestId } = renderWithRedux(
      renderWithRouter(
        <SearchProvider>
          <Enterprises />
        </SearchProvider>
      )
    );
    await waitFor(() => expect(fetchGet).toHaveBeenCalledTimes(1));
    expect(getAllByTestId('enterprise-card')).toHaveLength(31);
  });
  it('o input de filtro funciona corretamente para nomes e tipos de empresa', async () => {
    const {
      getByText,
      getByRole,
      getAllByRole,
      getAllByTestId,
    } = renderWithRedux(
      renderWithRouter(
        <SearchProvider>
          <Enterprises />
        </SearchProvider>
      )
    );
    await waitFor(() => expect(fetchGet).toHaveBeenCalledTimes(1));
    const searchButton = getAllByRole('button')[1];
    fireEvent.click(searchButton);
    const searchInput = getByRole('textbox');
    fireEvent.change(searchInput, { target: { value: 'real estate' } });
    expect(getAllByTestId('enterprise-card')).toHaveLength(3);
    fireEvent.change(searchInput, { target: { value: 'fintech' } });
    expect(getAllByTestId('enterprise-card')).toHaveLength(3);
    fireEvent.change(searchInput, { target: { value: 'health' } });
    expect(getAllByTestId('enterprise-card')).toHaveLength(2);
    fireEvent.change(searchInput, { target: { value: 'spa' } });
    expect(getAllByTestId('enterprise-card')).toHaveLength(10);
    fireEvent.change(searchInput, { target: { value: 'CryptoMkt' } });
    expect(getAllByTestId('enterprise-card')).toHaveLength(1);
    fireEvent.change(searchInput, {
      target: { value: 'NomeOuTipoNãoExistente' },
    });
    expect(
      getByText('Nenhuma empresa foi encontrada para a busca realizada.')
    ).toBeInTheDocument();
  });
});
