import React from 'react';
import { fireEvent, waitFor } from '@testing-library/react';
import renderWithRedux from '../helpers/renderWithRedux';
import renderWithRouter from '../helpers/renderWithRouter';
import Login from '../pages/Login';
import fetchPost from '../services/fetchPost';

afterEach(() => {
  fetchPost.mockClear();
});

const error = {
  message: 'Credenciais informadas são inválidas, tente novamente.',
};

jest.mock('../services/fetchPost');
fetchPost
  .mockImplementationOnce(() =>
    Promise.resolve({
      headers: {
        entries: () => [
          ['access-token', '123'],
          ['client', '546'],
          ['uid', '789'],
        ],
      },
    })
  )
  .mockImplementationOnce(() => Promise.reject(error));

describe('testes no Login', () => {
  it('inputs funcionam corretamente e botão enviar chama a api', async () => {
    const { getByText, container } = renderWithRedux(
      renderWithRouter(<Login />)
    );
    const [emailInput, passwordInput] = container.querySelectorAll('input');
    fireEvent.change(emailInput, {
      target: { value: 'testeapple@ioasys.com.br' },
    });
    fireEvent.change(passwordInput, { target: { value: '12341234' } });
    fireEvent.click(getByText('Entrar'));
    await waitFor(() => expect(fetchPost).toHaveBeenCalledTimes(1));
  });
  it('credenciais falsas retornam um erro', async () => {
    const { getByText, container } = renderWithRedux(renderWithRouter(<Login />));
    const [emailInput, passwordInput] = container.querySelectorAll('input');
    fireEvent.change(emailInput, {
      target: { value: 'usuarionaocadastrado@ioasys.com.br' },
    });
    fireEvent.change(passwordInput, { target: { value: 'QualquerSenha94' } });
    fireEvent.click(getByText('Entrar'));
    await waitFor(() => expect(fetchPost).toHaveBeenCalledTimes(1));
    expect(getByText(error.message)).toBeInTheDocument();
  });
});
