import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ children, className, onClick }) => (
  <button type="button" className={className} onClick={onClick}>
    {children}
  </button>
);

export default Button;

Button.defaultProps = {
  className: undefined,
  onClick: undefined,
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func,
};
