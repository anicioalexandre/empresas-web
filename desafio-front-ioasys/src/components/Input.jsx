import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ onChange, placeholder, type, className }) => (
  <input
    onChange={onChange}
    placeholder={placeholder}
    type={type}
    className={className}
  />
);

export default Input;

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};
