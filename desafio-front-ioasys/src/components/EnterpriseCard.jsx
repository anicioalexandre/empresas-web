import React from 'react';
import PropTypes from 'prop-types';
import Image from './Image';
import EnterpriseImage from '../images/e1.png';
import '../styles/Enterprises.css';

const EnterpriseCard = ({ info, className = 'cardContainer', details }) => (
  <div data-testid="enterprise-card" className={className}>
    <Image
      className="cardImage"
      height={`${100}%`}
      width={`${50}%`}
      src={EnterpriseImage}
      alt="enterprise logo"
    />
    <div className="namesContainer">
      <h4>{info.enterprise_name}</h4>
      <h5>{info.enterprise_type.enterprise_type_name}</h5>
      <h6>{info.country}</h6>
      {details && <article>{info.description}</article>}
    </div>
  </div>
);

export default EnterpriseCard;

EnterpriseCard.defaultProps = {
  className: 'cardContainer',
  details: false,
};

EnterpriseCard.propTypes = {
  info: PropTypes.shape({
    enterprise_name: PropTypes.string,
    enterprise_type: PropTypes.shape({
      enterprise_type_name: PropTypes.string,
    }),
    country: PropTypes.string,
    info: PropTypes.number,
    description: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  details: PropTypes.bool,
};
