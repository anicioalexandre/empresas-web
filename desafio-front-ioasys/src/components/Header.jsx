import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import Image from './Image';
import '../styles/Header.css';
import ioasysWhite from '../images/ioasysWhite.png';
import searchIcon from '../images/search.png';
import Button from './Button';
import SearchInput from './SearchInput';
import BackIcon from '../images/backIcon.png';

const Header = ({ previousPage }) => {
  const history = useHistory();
  const [activeSearch, setActiveSearch] = useState(false);
  return (
    <header className="headerContainer">
      <Button onClick={() => history.push(previousPage)}>
        <Image src={BackIcon} alt="left arrow" />
      </Button>

      {!activeSearch ? (
        <Image
          className="headerImage"
          width={`${200}px`}
          src={ioasysWhite}
          alt="ioasys logo white"
        />
      ) : (
        <SearchInput />
      )}

      <Button
        onClick={() => setActiveSearch(!activeSearch)}
        className="searchButton"
      >
        <Image src={searchIcon} alt="search icon" />
      </Button>
    </header>
  );
};

export default Header;

Header.propTypes = {
  previousPage: PropTypes.string.isRequired,
};
