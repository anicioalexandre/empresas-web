import React from 'react';
import PropTypes from 'prop-types';

const Image = ({ alt, className, src, width, height }) => (
  <img
    alt={alt}
    className={className}
    src={src}
    width={width}
    height={height}
  />
);

export default Image;

Image.defaultProps = {
  className: '',
  width: undefined,
  height: undefined,
};

Image.propTypes = {
  alt: PropTypes.string.isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
  src: PropTypes.string.isRequired,
  className: PropTypes.string,
};
