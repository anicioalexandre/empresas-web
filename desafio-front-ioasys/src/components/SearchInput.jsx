import React, { useContext } from 'react';
import Input from './Input';
import { SearchContext } from '../context/SearchContext';

const SearchInput = () => {
  const { setSearchInput } = useContext(SearchContext);
  return (
    <Input
      type="text"
      onChange={(e) => setSearchInput(e.target.value)}
      className="searchInput"
      placeholder="Pequise por nome ou tipo da empresa"
    />
  );
};

export default SearchInput;
