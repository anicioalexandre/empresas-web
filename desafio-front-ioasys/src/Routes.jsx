import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './pages/Login';
import Enterprises from './pages/Enterprises';
import SearchProvider from './context/SearchContext';
import EnterpriseDetails from './pages/EnterpriseDetails';
import NotFound from './pages/NotFound';

const Routes = () => {
  return (
    <Switch>
      <Route path="/empresas/:id" component={EnterpriseDetails} />
      <Route path="/empresas">
        <SearchProvider>
          <Enterprises />
        </SearchProvider>
      </Route>
      <Route exact path="/" component={Login} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
