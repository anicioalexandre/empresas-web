import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import EnterpriseCard from '../components/EnterpriseCard';
import { getLocalStorage } from '../services/localStorage';
import { getEnterprises, cleanError } from '../redux/actions/enterprises';
import ioasysLogo from '../images/ioasysLogo.png';
import '../styles/EnterpriseDetails.css';
import '../styles/Enterprises.css';
import '../styles/Login.css';
import Image from '../components/Image';
import Header from '../components/Header';

const EnterpriseDetails = ({
  getEnterprisesAPI,
  enterprisesList,
  loadingEnterprises,
  error,
  cleanErrorUnMount,
}) => {
  const { id } = useParams();
  useEffect(() => {
    const localTokens = getLocalStorage('tokens');
    if (enterprisesList.length === 0)
      getEnterprisesAPI('enterprises', localTokens);

    return () => cleanErrorUnMount();
  }, []);
  const getEnterpriseDetails = (enterpriseId) => {
    const selectedEnterprise = enterprisesList.find(
      (enterprise) => enterprise.id === Number(enterpriseId)
    );
    return selectedEnterprise;
  };
  return (
    <div className="appRoute">
      <Header previousPage="/empresas" />
      <div className="cardsContainer">
        {error && <h4>{error.errors}</h4>}
        {loadingEnterprises && (
          <Image
            width={`${10}%`}
            className="loadingMessage"
            src={ioasysLogo}
            alt="ioasys logo"
          />
        )}
        {enterprisesList.length > 0 && (
          <EnterpriseCard
            className="detailsCard"
            info={getEnterpriseDetails(id)}
            details
          />
        )}
      </div>
    </div>
  );
};

const mapState = (state) => ({
  customHeaders: state.login.customHeaders,
  enterprisesList: state.enterprises.enterprisesList,
  loadingEnterprises: state.enterprises.loadingEnterprises,
  error: state.enterprises.error,
});

const mapDispatch = {
  getEnterprisesAPI: getEnterprises,
  cleanErrorUnMount: cleanError,
};

export default connect(mapState, mapDispatch)(EnterpriseDetails);

EnterpriseDetails.defaultProps = {
  enterprisesList: [],
  error: undefined,
};

EnterpriseDetails.propTypes = {
  getEnterprisesAPI: PropTypes.func.isRequired,
  cleanErrorUnMount: PropTypes.func.isRequired,
  loadingEnterprises: PropTypes.bool.isRequired,
  error: PropTypes.objectOf(PropTypes.string),
  enterprisesList: PropTypes.arrayOf(PropTypes.object),
};
