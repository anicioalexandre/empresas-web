import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PassIcon from '../svg/PassIcon';
import EmailIcon from '../svg/EmailIcon';
import Input from '../components/Input';
import Button from '../components/Button';
import '../styles/Login.css';
import { getLogin } from '../redux/actions/login';
import Image from '../components/Image';
import ioasysLogo from '../images/ioasysLogo.png';
import ioasysFullLogo from '../images/ioasysFullLogo.png';
import VisibleIcon from '../svg/VisibleIcon';
import UnvisibleIcon from '../svg/UnvisibleIcon';

const Login = ({ getLoginAPI, loadingLogin, customHeaders, error }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loadingEffect, setLoadingEffect] = useState('');
  const [loginValidation, setLoginValidation] = useState('');
  const [passwordVisibility, setPasswordVisibility] = useState(false);
  useEffect(() => {
    if (error) {
      setLoginValidation('is-invalid');
      setLoadingEffect('');
    }
  }, [error]);
  return (
    <>
      {loadingLogin && (
        <Image
          width={`${5}%`}
          className="loadingMessage"
          src={ioasysLogo}
          alt="ioasys logo"
        />
      )}
      {customHeaders && <Redirect to="/empresas" />}
      <form className={`formContainer ${loadingEffect}`}>
        <div className="title">
          <Image width={`${70}%`} src={ioasysFullLogo} alt="ioasys logo" />
          <h4>App empresas</h4>
        </div>
        <div className="form-group ">
          <div className="input-group mb-0 emailSize">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <EmailIcon />
              </span>
            </div>
            <Input
              className={`form-control ${loginValidation}`}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="E-mail"
              type="email"
            />
          </div>
        </div>
        <div className="form-group">
          <div className="input-group mb-0">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <PassIcon />
              </span>
            </div>
            <Input
              className={`form-control ${loginValidation}`}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Senha"
              type={passwordVisibility ? 'text' : 'password'}
            />
            <div className="input-group-prepend">
              <Button
                onClick={() => setPasswordVisibility(!passwordVisibility)}
                className="btn shadow-none"
              >
                {passwordVisibility ? <UnvisibleIcon /> : <VisibleIcon />}
              </Button>
            </div>
          </div>
          {error && (
            <small className="form-text text-danger tooltipText">
              Credenciais informadas são inválidas, tente novamente.
            </small>
          )}
        </div>
        <Button
          onClick={() => {
            getLoginAPI('users/auth/sign_in', { email, password });
            setLoadingEffect('loadingEffect');
          }}
          className="btn btn-danger form-control"
        >
          Entrar
        </Button>
      </form>
    </>
  );
};

const mapState = (state) => ({
  customHeaders: state.login.customHeaders,
  loadingLogin: state.login.loadingLogin,
  error: state.login.error,
});

const mapDispatch = {
  getLoginAPI: getLogin,
};

export default connect(mapState, mapDispatch)(Login);

Login.defaultProps = {
  customHeaders: null,
  error: undefined,
};

Login.propTypes = {
  loadingLogin: PropTypes.bool.isRequired,
  getLoginAPI: PropTypes.func.isRequired,
  customHeaders: PropTypes.objectOf(PropTypes.string),
  error: PropTypes.objectOf(PropTypes.string),
};
