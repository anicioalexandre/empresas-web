import React, { useEffect, useContext } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getEnterprises, cleanError } from '../redux/actions/enterprises';
import { setLocalStorage, getLocalStorage } from '../services/localStorage';
import EnterpriseCard from '../components/EnterpriseCard';
import '../styles/Enterprises.css';
import '../styles/Login.css';
import ioasysLogo from '../images/ioasysLogo.png';
import Header from '../components/Header';
import Image from '../components/Image';
import filterEnterprisesNamesOrTypes from '../services/filterEnterprisesNamesOrTypes';
import { SearchContext } from '../context/SearchContext';

const Enterprises = ({
  getEnterprisesAPI,
  customHeaders,
  enterprisesList,
  loadingEnterprises,
  error,
  cleanErrorUnMount,
}) => {
  useEffect(() => {
    if (customHeaders) setLocalStorage('tokens', customHeaders);
  }, [customHeaders]);
  useEffect(() => {
    const localTokens = getLocalStorage('tokens');
    if (enterprisesList.length === 0)
      getEnterprisesAPI('enterprises', localTokens);

    return () => cleanErrorUnMount();
  }, []);

  const { searchInput } = useContext(SearchContext);

  return (
    <div className="appRoute">
      <Header previousPage="/" />
      <div className="cardsContainer">
        {error && <h4>{error.errors}</h4>}
        {loadingEnterprises && (
          <Image
            width={`${10}%`}
            className="loadingMessage"
            src={ioasysLogo}
            alt="ioasys logo"
          />
        )}
        {filterEnterprisesNamesOrTypes(enterprisesList, searchInput).map(
          (enterpriseInfo) => (
            <Link key={enterpriseInfo.id} to={`empresas/${enterpriseInfo.id}`}>
              <EnterpriseCard info={enterpriseInfo} />
            </Link>
          )
        )}
        {filterEnterprisesNamesOrTypes(enterprisesList, searchInput).length ===
          0 &&
          !loadingEnterprises &&
          !error && (
            <h4>Nenhuma empresa foi encontrada para a busca realizada.</h4>
          )}
      </div>
    </div>
  );
};

const mapState = (state) => ({
  customHeaders: state.login.customHeaders,
  enterprisesList: state.enterprises.enterprisesList,
  loadingEnterprises: state.enterprises.loadingEnterprises,
  error: state.enterprises.error,
});

const mapDispatch = {
  getEnterprisesAPI: getEnterprises,
  cleanErrorUnMount: cleanError,
};

export default connect(mapState, mapDispatch)(Enterprises);

Enterprises.defaultProps = {
  customHeaders: null,
  enterprisesList: [],
  error: undefined,
};

Enterprises.propTypes = {
  getEnterprisesAPI: PropTypes.func.isRequired,
  cleanErrorUnMount: PropTypes.func.isRequired,
  loadingEnterprises: PropTypes.bool.isRequired,
  error: PropTypes.objectOf(PropTypes.string),
  customHeaders: PropTypes.objectOf(PropTypes.string),
  enterprisesList: PropTypes.arrayOf(PropTypes.object),
};
