import React from 'react';
import '../styles/Enterprises.css';
import Header from '../components/Header';

const NotFound = () => (
  <div className="appRoute">
    <Header previousPage="/" />
    <div className="cardsContainer">
      <h3>Página não encontrada.</h3>
    </div>
  </div>
);

export default NotFound;
