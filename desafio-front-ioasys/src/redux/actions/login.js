import fetchPost from '../../services/fetchPost';

export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const REQUEST_LOGIN_SUCCESS = 'REQUEST_LOGIN_SUCCESS';
export const REQUEST_LOGIN_FAILURE = 'REQUEST_LOGIN_FAILURE';

const requestLogin = () => ({
  type: REQUEST_LOGIN,
});
const requestLoginSuccess = ({ 'access-token': accessToken, client, uid }) => ({
  type: REQUEST_LOGIN_SUCCESS,
  customHeaders: { accessToken, client, uid },
});
const requestLoginFailure = (error) => ({
  type: REQUEST_LOGIN_FAILURE,
  error,
});

export const getLogin = (endpoint, loginObj) => {
  return (dispatch) => {
    dispatch(requestLogin());
    return fetchPost(endpoint, loginObj).then(
      (data) => {
        const customHeaders = {};
        for (const [key, value] of data.headers.entries()) {
          customHeaders[key] = value;
        }
        dispatch(requestLoginSuccess(customHeaders));
      },
      (error) => dispatch(requestLoginFailure(error))
    );
  };
};
