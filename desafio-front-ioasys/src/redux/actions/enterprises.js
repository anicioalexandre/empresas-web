import fetchGet from '../../services/fetchGet';

export const REQUEST_ENTERPRISES = 'REQUEST_ENTERPRISES';
export const REQUEST_ENTERPRISES_SUCCESS = 'REQUEST_ENTERPRISES_SUCCESS';
export const REQUEST_ENTERPRISES_FAILURE = 'REQUEST_ENTERPRISES_FAILURE';
export const CLEAN_ERROR = 'CLEAN_ERROR';

const requestEnterprises = () => ({
  type: REQUEST_ENTERPRISES,
});
const requestEnterprisesSuccess = (enterprisesList) => ({
  type: REQUEST_ENTERPRISES_SUCCESS,
  enterprisesList,
});
const requestEnterprisesFailure = (error) => ({
  type: REQUEST_ENTERPRISES_FAILURE,
  error,
});

export const cleanError = () => ({
  type: CLEAN_ERROR,
});

export const getEnterprises = (endpoint, customHeaders) => {
  return (dispatch) => {
    dispatch(requestEnterprises());
    return fetchGet(endpoint, customHeaders).then(
      (enterprisesData) =>
        dispatch(requestEnterprisesSuccess(enterprisesData.enterprises)),
      (error) => dispatch(requestEnterprisesFailure(error))
    );
  };
};
