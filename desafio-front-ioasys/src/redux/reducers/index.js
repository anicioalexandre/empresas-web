import { combineReducers } from 'redux';
import login from './login';
import enterprises from './enterprises';

export default combineReducers({ login, enterprises });
