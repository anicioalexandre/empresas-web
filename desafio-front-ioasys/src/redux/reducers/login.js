import {
  REQUEST_LOGIN,
  REQUEST_LOGIN_SUCCESS,
  REQUEST_LOGIN_FAILURE,
} from '../actions/login';

const INITIAL_STATE = {
  loadingLogin: false,
  customHeaders: null,
};

const login = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_LOGIN:
      return { ...state, loadingLogin: true };
    case REQUEST_LOGIN_SUCCESS:
      return {
        ...state,
        loadingLogin: false,
        customHeaders: action.customHeaders,
      };
    case REQUEST_LOGIN_FAILURE:
      return { ...state, loadingLogin: false, error: action.error };
    default:
      return state;
  }
};

export default login;
