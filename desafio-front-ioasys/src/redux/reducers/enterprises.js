import {
  REQUEST_ENTERPRISES,
  REQUEST_ENTERPRISES_SUCCESS,
  REQUEST_ENTERPRISES_FAILURE,
  CLEAN_ERROR,
} from '../actions/enterprises';

const INITIAL_STATE = {
  loadingEnterprises: false,
  enterprisesList: [],
};

const enterprises = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_ENTERPRISES:
      return { ...state, loadingEnterprises: true };
    case REQUEST_ENTERPRISES_SUCCESS:
      return {
        ...state,
        loadingEnterprises: false,
        enterprisesList: action.enterprisesList,
      };
    case REQUEST_ENTERPRISES_FAILURE:
      return { ...state, loadingEnterprises: false, error: action.error };
    case CLEAN_ERROR:
      return { ...state, loadingEnterprises: false, error: undefined };
    default:
      return state;
  }
};

export default enterprises;
